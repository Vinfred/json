package com.test.mirka.json.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.squareup.picasso.Picasso;
import com.test.mirka.json.R;
import com.test.mirka.json.model.Content;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
	private final String LOG_TAG = MainActivity.class.getSimpleName();
	public static final String ARG_PHOTOS = "photos";
	public static final String ARG_INDEX = "ind";

	private TextView dateView;
	private TextView textView;
	private TextView titleView;


	private final static String JSON_URI = "http://api.naij.com/test.json";

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		dateView = (TextView) findViewById(R.id.date_view);
		titleView = (TextView) findViewById(R.id.title_view);

		new FetchData().execute(JSON_URI);
	}

	private class FetchData extends AsyncTask<String, Void, Content> {
		private String text;
		private ArrayList<String> pics;
		private LinearLayout layout;


		@Override
		protected Content doInBackground (String... params) {
			if (params.length == 0) {
				return null;
			}

			HttpURLConnection urlConnection = null;
			BufferedReader reader = null;

			String jsonStr = null;

			try {
				URL url = new URL(params[0]);

				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.connect();

				InputStream inputStream = urlConnection.getInputStream();
				StringBuffer buffer = new StringBuffer();
				if (inputStream == null) {
					// Nothing to do.
					return null;
				}
				try {
					reader = new BufferedReader(new InputStreamReader(inputStream));
					String line;
					while ((line = reader.readLine()) != null) {
						buffer.append(line + "\n");
					}

					if (buffer.length() == 0) {
						// Stream was empty.  No point in parsing.
						return null;
					}
					jsonStr = buffer.toString();
				}

				catch (Exception e) {
					Log.e(LOG_TAG, "Error ", e);
					return null;
				}
			}
			catch (IOException e) {
				Log.e(LOG_TAG, "Error ", e);
				return null;
			}

			finally {
				if (urlConnection != null) {
					urlConnection.disconnect();
				}
				if (reader != null) {
					try {
						reader.close();
					}
					catch (final IOException e) {
						Log.e(LOG_TAG, "Error closing stream", e);
					}
				}
			}

			try {
				return getDataFromJson(jsonStr);
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}


		@Override
		protected void onPostExecute (Content c) {
			if (c == null) {
				return;
			}
			else {
				titleView.setText(c.getTitle());
				dateView.setText(c.getReadableDateString());

				layout = (LinearLayout) findViewById(R.id.content_layout);

				List<Pair> content = c.getParsedContent();
				pics = new ArrayList<>();
				for (Pair p : content) {
					final Content.Type type = (Content.Type) p.first;
					final String value = p.second.toString();

					if (type.equals(Content.Type.IMAGE)) {
						addImage(value);
					}
					else if (type.equals(Content.Type.VIDEO)) {
						addVideoFragment(value);
					}
					else {
						addText(value);
					}
				}
			}
		}

		private void addText(final String value) {
			TextView text = new TextView(MainActivity.this);
			text.setText(value);
			text.setTextSize(getResources().getDimension(R.dimen.body1_textsize));
			layout.addView(text);
		}

		private void addImage (final String value) {
			pics.add(value);
			ImageView image = new ImageView(MainActivity.this);
			LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams
					(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

			image.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick (View v) {
					Intent i = new Intent(MainActivity.this, PhotoActivity.class);
					i.putExtra(ARG_PHOTOS, pics);
					i.putExtra(ARG_INDEX, pics.indexOf(value));
					startActivity(i);
				}
			});

			Picasso.with(MainActivity.this)
					.load(value)
					.into(image);
			layout.addView(image, layoutParam);
		}

		private void addVideoFragment (final String value) {
			FrameLayout fl = new FrameLayout (MainActivity.this);
			LinearLayout.LayoutParams layoutParam = new
					LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			layoutParam.setMargins(0, (int) getResources().getDimension(R.dimen.activity_vertical_margin), 0,
					(int) getResources().getDimension(R.dimen.activity_vertical_margin));
			fl.setId(R.id.video_layout);

			YouTubePlayerSupportFragment youTubeFragment = YouTubePlayerSupportFragment.newInstance();
			youTubeFragment.initialize(getResources().getString(R.string.api_key), new YouTubePlayer.OnInitializedListener() {
				@Override
				public void onInitializationSuccess (YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
					String[] tokens = value.split("/");

					player.loadVideo(tokens[tokens.length - 1]);
					player.pause();
				}

				@Override
				public void onInitializationFailure (YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
					Log.e(LOG_TAG, youTubeInitializationResult.toString());
				}
			});

			getSupportFragmentManager().beginTransaction().add(fl.getId(), youTubeFragment).commit();

			layout.addView(fl, layoutParam);
		}

		private Content getDataFromJson (String jsonStr) throws JSONException {
			final String TITLE = "title";
			final String CONTENT = "content";
			final String DATE = "created";

			JSONObject jsonObject = new JSONObject(jsonStr);

			return new Content(jsonObject.get(TITLE).toString(), jsonObject.get(CONTENT).toString(),
					jsonObject.get(DATE).toString());
		}
	}
}
