package com.test.mirka.json.model;

import android.support.v4.util.Pair;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by mirka on 19.03.15.
 */
public class Content {
	private String title;
	private String content;
	private String date;

	private final static String TIME_ZONE = "Africa/Lagos";
	private final static String DATE_FORMAT = "MMMM dd, yyyy HH:mm";
	private final static int MILLIS_IN_SECOND = 1000;

	public enum Type {TEXT, IMAGE, VIDEO};
	private final String IMG_TAG = "img";
	private final String VIDEO_TAG = "iframe";
	private final String TEXT_TAG = "p";

	public Content (String title, String content, String date) {
		this.title = title;
		this.content = content;
		this.date = date;
	}

	public Content () {
	}


	public String getTitle () {
		return title;
	}

	public void setTitle (String title) {
		this.title = title;
	}

	public String getContent () {
		return content;
	}

	public void setContent (String content) {
		this.content = content;
	}

	public List<Pair> getParsedContent () {
		Document doc = Jsoup.parse(content);
		List<Pair> content = new LinkedList<>();

		Elements elements = doc.body().select("*");
		for (Element e : elements) {
			if (e.tagName().equals(IMG_TAG)) {
				content.add(new Pair(Type.IMAGE, e.attr("abs:src")));
			}
			else if (e.tagName().equals(VIDEO_TAG)) {
				content.add(new Pair(Type.VIDEO, e.attr("src")));
			}
			else if (e.tagName().equals(TEXT_TAG)) {
				content.add(new Pair(Type.TEXT, e.text()));
			}
		}

		return content;
	}


	public String getDate () {
		return date;
	}

	public String getReadableDateString () {
		SimpleDateFormat shortenedDateFormat = new SimpleDateFormat(DATE_FORMAT);
		shortenedDateFormat.setTimeZone(TimeZone.getTimeZone(TIME_ZONE));
		return shortenedDateFormat.format(Long.parseLong(date)*MILLIS_IN_SECOND);
	}

	public void setDate (String date) {
		this.date = date;
	}
}
